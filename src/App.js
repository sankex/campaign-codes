import React, { Component } from 'react';
//import logo from './logo.svg';
import CampaignCodes from './components/CampaignCodes'

class App extends Component {
  render() {
    return (
      <section className="section">
        <div className="container">
          <CampaignCodes />
        </div>
      </section>
    );
  }
}

export default App;
