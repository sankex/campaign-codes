import React, { Component } from 'react'

export default class CampaignCodes extends Component {
    constructor() {
        super();
        this.state = {
            baseURL: '',
            source: '',
            campaign: '',
            content: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (evt) {
        // check it out: we get the evt.target.name (which will be either "email" or "password")
        // and use it to target the key on our `state` object with the same name, using bracket syntax
        this.setState({ [evt.target.name]: evt.target.value });
      }

    render() {
        const {baseURL, source, campaign, content} = this.state
        const utm_source = '?utm_source='
        const utm_medium = '&utm_medium=email'
        const utm_campaign = '&utm_campaign='
        const utm_content = '&utm_content='
        return (
            <div>
                <div className="columns is-centered">
                    <div className="column is-half">
                        <h1 className="is-size-3">Campaign Code Generator</h1>
                        <p>Provide a URL, utm_source, utm_campaign and utm_content (These should be provided to you) and the form will generate a full URL with the correct campaign code attached</p>
                        <form>
                        <div className="field">
                                <label className="label">Base URL</label>
                                <div className="control">
                                <input type='text' name='baseURL' onChange={this.handleChange} className="input"/>
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">utm_source</label>
                                <div className="control">
                                <input type='text' name='source' onChange={this.handleChange} className="input"/>
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">utm_campaign</label>
                                <div className="control">
                                <input type='text' name='campaign' onChange={this.handleChange} className="input"/>
                                </div>
                            </div>
                            <div className="field">
                                <label className="label">utm_content</label>
                                <div className="control">
                                <input type='text' name='content' onChange={this.handleChange} className="input"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="columns is-centered">
                    <div className="column is-half">
                    <p>Generated URL:</p>
                    {baseURL}{utm_source}{source}{utm_medium}{utm_campaign}{campaign}{utm_content}{content}
                    </div>
                </div>
            </div>
        );
    }
}