import React, { Component } from 'react'

class Card extends Component {
    render() {
        const style = {
            padding: 20, 
            color: 'white',
            backgroundColor: this.props.color,
            width: 350,
            textAlign: 'center',
        }

        return(
            <div style={style}>
                {this.props.children}
            </div>
        )
    }
}

export default Card;