import React, { Component } from 'react'

export default class Input extends Component {
    state = {value: ''}

    handleChange = (e) => {
        this.setState({value: e.target.value})
    }

    render() {
        const {value} = this.state
        
        return(
            <div>
                <label htmlFor={'id'}>
                    Enter Value
                </label>
                <input
                    id={'id'}
                    type={'text'}
                    value={value}
                    placeholder={'Placeholder'}
                    onChange={this.handleChange}
                />
                <br/>
                <br />
                My Value: {value}
            </div>
        )
    }
}